# Room Control
Control the room by GPIO pins and communicating with other devices over mqtt.

# Requirements
Check out the requirements of [mqtt_node_paho](https://gitlab.com/NANDLAB/mqtt_node_paho).

Also install these libraries with apt:
```
sudo apt install libola-dev libboost-program-options-dev
```

# Configure OLA
```
sudo sed -i.bak /^enabled/d /etc/ola/*.conf
```
Now you can either disable all features OR enable only FTDI.
## Disable all features
```
echo "enabled = false" | sudo tee -a /etc/ola/*.conf
```
## Enable FTDI only
```
for f in /etc/ola/*.conf ; do
    case "$f" in
        /etc/ola/ola-ftdidmx.conf)
            echo "enabled = true" | sudo tee -a "$f" > /dev/null
            ;;
        *)
            echo "enabled = false" | sudo tee -a "$f" > /dev/null
            ;;
    esac
done
```
## Revert to backups
```
for f in /etc/ola/*.conf.bak ; do
    sudo mv "$f" "${f%\.bak}"
done
```

# Build and install DEB package
```
git clone --recursive https://gitlab.com/NANDLAB/room_control_paho
cd room_control_paho
mkdir build
cd build
cmake .. -DRC_BUILD_DEB=ON
make
cpack
sudo apt install ./*.deb
```

# Enable autostart on boot
```
sudo systemctl enable room_control_paho
```

# Usage
To see all the program options enter:
```
./room_control_paho --help
```
